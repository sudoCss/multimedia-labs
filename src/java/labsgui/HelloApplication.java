package labsgui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import org.apache.commons.math3.analysis.function.Abs;
import org.opencv.core.Core;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class HelloApplication extends Application {
	static {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	}

	@Override
	public void start(Stage stage) throws IOException {
		BufferedImage img;
		try {
			img = ImageIO.read(new File("sudoCss.png"));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		System.out.println(img.getData().getSample(300, 300, 0));
		int[] iarr = img.getData().getPixel(300, 300, new int[]{1, 2, 3});
		System.out.println(iarr[0] + " " + iarr[1] + " " + iarr[2]);

		Abs abs = new Abs();
		System.out.println(abs.value(-10.0d));

		FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("hello-view.fxml"));
		Scene scene = new Scene(fxmlLoader.load(), 320, 240);
		stage.setTitle("Hello!");
		stage.setScene(scene);
		stage.show();
	}

	public static void main(String[] args) {
		launch();
	}
}
